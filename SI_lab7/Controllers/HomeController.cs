﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Renci.SshNet;
using SI_lab7.Models;

namespace SI_lab7.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ExecuteWinCommand(string command)
        {
            var p = new Process
            {
                StartInfo =
                {
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                    FileName = "cmd.exe",
                    Arguments = "/c " + $"{command}",
                    //Verb = "runas",
                    UseShellExecute = false,
                }
            };
            p.Start();
            var output = await p.StandardOutput.ReadToEndAsync();
            p.WaitForExit(2000);

            return Ok(output);
        }

        [HttpGet]
        public async Task<IActionResult> ExecuteLinuxCommand(string command)
        {
            string fileName;
            if (Environment.Is64BitProcess)
                // this works on x64
                fileName = "bash.exe";
            else
            {
                // this works on x86
                fileName = Environment.GetEnvironmentVariable("WinDir") + "\\SysNative\\bash.exe";
            }

            var p = new Process
            {
                StartInfo =
                {
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    FileName = fileName,
                    UseShellExecute = false,
                }
            };
            p.Start();
            await p.StandardInput.WriteLineAsync(command);
            p.StandardInput.Close();
            var output = await p.StandardOutput.ReadToEndAsync();
            //p.WaitForExit(2000);
            return Ok(output);
        }


        [HttpGet]
        public async Task<IActionResult> ExecuteSSHCommand(string command)
        {
            var sshClient = new SshClient("172.20.227.235", "vm1", "111111");
            sshClient.Connect();
            var sc = sshClient.CreateCommand("echo '111111' | sudo -S " + command);
            sc.Execute();
            var output = sc.Result;
            return Ok(output);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}